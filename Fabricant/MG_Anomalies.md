# Anomalies #

> Ce chapitre détaille les anomalies que vous pourriez constater lors de l'importation de contenu dans la plate-forme.

## 1 - Import ##

### 1.1 - Import / Setup Import ###

#### Fichiers xls / Error ####

Vérifier qu'il s'agit bien d'un fichier xls.

#### Media / Error ####

Contacter le Support utilisateur Kialy.

### 1.2 - Import / Dry-run import ###

#### Error / Ligne vide ####

Ligne du fichier xls d'import indiquée est vide.

- Supprimer la ligne dans le fichier xls d'import.
- Supprimer le fichier xls dans Import / Setup Import.
- Uploadé le fichier xls corrigé.

**Vous ne pouvez pas importer du contenu avec cette erreur.**

#### Error / Code article absent ####

Le code article manque dans la ligne du fichier xls d'import indiquée.

- Ajoutez le code article dans la ligne dans le fichier xls d'import.
- Supprimer le fichier xls dans Import / Setup Import.
- Uploadé le fichier xls corrigé.

**Vous ne pouvez pas importer du contenu avec cette erreur.**

#### Error / Colonne absente ####

La colonne obligatoire manque dans le fichier xls d'import.

- Ajoutez la colonne dans le fichier xls d'import.
- Supprimer le fichier xls dans Import / Setup Import.
- Uploadé le fichier xls corrigé.

**Vous ne pouvez pas importer du contenu avec cette erreur.**

#### Warning / Media absent ####

Il manque un media à l'article dans la ligne du fichier xls d'import indiquée.

- Vérifier dans la liste des medias de Import / Setup Import que le media est présent,
- Sinon téléchargez le media dans la fenêtre d'import des medias de Import / Setup Import et relancer un Dry-Run.

- Vérifier que le nom du media (extension incluse) est strictement identique dans le fichier xls d'import.
- Sinon indiquer le nom du media dans le fichier xls + supprimer le fichier xls de Import / Setup Import + téléchargez le fichier xls conforme dans la fenêtre Import / Setup Import + relancer un Dry-Run.

**Vous pouvez ne pas prendre en compte ce warning, alors le media concerné ne sera pas importé dans la plate-forme.**

### 1.3 - Import / Report ###

Un artcile n'a pas le statut Uploaded. Contacter le Support client Kialy.

## 2 - Téléchargement (upload) ##

#### Fichier .zip corrompu ####

- Relancer le download.

#### Fichier .zip vide ####

- Vérifier les fiches des articles de la sélection.
- Si toujours KO contacter le Support utilisateur.

#### Manque un article dans le fichier .zip vide ####

- Vérifier la sélection.
- Si toujours KO contacter le Support utilisateur.

#### Manque un media dans le fichier .zip vide ####

- Vérifier les fiches des articles de la sélection.
- Si toujours KO contacter le Support utilisateur.

## 3 - Envoi ##

#### Pas de fichier .zip ####

- Relancer le **Send**,
- Si toujours KO contacter le Support utilisateur.

#### Fichier .zip corrompu ####

- Relancer le **Send**.
- Si toujours KO contacter le Support utilisateur.

#### Fichier .zip vide ####

- Vérifier les fiches des articles de la sélection.
- Vérifier que l'utilisateur destinataire possède bien les permissions sur l'article.
- Si toujours KO contacter le Support utilisateur.

#### Manque un article dans le fichier .zip vide ####

- Vérifier la sélection.
- Vérifier que l'utilisateur destinataire possède bien les permissions sur l'article.
- Si toujours KO contacter le Support utilisateur.

#### Manque un media dans le fichier .zip vide ####

- Vérifier les fiches des articles de la sélection.
- Vérifier que l'utilisateur destinataire possède bien les permissions sur l'article.
- Si toujours KO contacter le Support utilisateur.
