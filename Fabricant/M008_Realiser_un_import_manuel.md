# Réaliser un import manuel #

## 1. Introduction ##

> Ce chapitre détaille les actions à réaliser concernant l'importation manuelle de contenu dans Kialy.

L'import peut être utilisée pour :

- Créer une marque,
- Créer une nouvelle classification,
- Compléter une classification existante,
- Créer, supprimer un(des) article(s),
- Créer, modifier, supprimer une(des) données(s),
- Créer, modifier, supprimer un(des) media(s).

Un import de contenu est considéré comme un projet qui peut être démarré, mis en attente, compléter de plusieurs fichiers de données et de plusieurs medias. 

L'accès à tous les projets d'imports pour un société se fait par le menu **IMPORT** de la plate-forme.

L'import se déroule en 3 phases :

- Télécharger les fichiers de données et medias,
- Tester l'importation des données et des medias,
- Importer effectivement les données et les medias.

Tout projet d'import peut-être mis en attente ou supprimé dès lors que vous n'avez pas lancé l'importation effective du contenu via **Run import**.

## 2. Méthode ##

### 2.1 - Télécharger les fichiers ###
- Connectez-vous sur **https://kly.io/login**,
- Entrez dans le menu **Import**,
- Cliquez **Create New import**,
- Vous êtes alors dans l'onglet **Setup Import**.

#### Télécharger les données ####

- Déplacer votre(vos) fichier(s) d'import dans **1.Add XLS files**,
- Le téléchargement est terminé lorsque chacun des fichiers possède un statut.

        Si tous les fichiers n'ont pas le statut Uploaded : nous vous recommandons de vous référer à la rubrique Anomalies d'import.

#### Télécharger les medias ####

- Déplacer vos fichiers medias dans **2.Add media files**,
- Attendre que la plate-forme télécharge tous les medias,
- Le téléchargement est terminé lorsque chacun des medias possède un statut.

        Si tous les medias n'ont pas le statut Uploaded : référez-vous à la rubrique Anomalies d'import.

Si le rapport vous convient vous pouvez passer à l'étape suivante.

### 2.2 - Tester l'import ###

- Cliquez **Dry-run import**,
- La plate-forme teste le(s) fichier(s) de données ainsi que le lien avec les medias téléchargés,
- Le test est terminé lorsque chacun des articles (sku) possède un statut,
- Vous êtes alors dans l'onglet **DryRun Log**.

        Si tous les articles n'ont pas le statut Success : référez-vous à la rubrique Anomalies d'import.

A ce stade, le contenu n'a toutjours été importé donc votre contenu n'a pas encore été modifié.

Si le rapport vous convient vous pouvez passer à l'étape suivante.

### 2.3 - Importer le contenu ###

- Cliquez **Run import**,
- La plate-forme importe définitivement le contenu,
- L'import est terminé lorsque chacun des articles (sku) possède un statut,
- Vous êtes alors dans l'onglet **Report**,


        Si tous les articles n'ont pas le statut Success : référez-vous à la rubrique Anomalies d'import.

Vous pouvez constater la présence des articles dans **Catalog**.

Le projet d'import possède le statut **Import done** dans **Import**.

### 2.4 - Mettre en attente un projet d'import ###

Quittez le processus d'import à l'étape souhaitée, en cliquant sur l'un des menus de Kialy, et surtout avant de cliquer sur **Run Import**.

Le projet d'import possèdera alors un statut :

- **New** : projet d'import arrêté à l'étape "Télécharger les fichiers",
- **Dry-Run done** : projet d'import arrêté après l'étape "Tester l'import".

### 2.5 - Reprendre un projet d'import ###

- Cliquez sur **View details** du projet d'import concerné,
- Poursuivez le processus.

### 2.6 - Supprimer un projet d'import ###

- Cliquez sur **Delete** du projet d'import concerné,
- Confirmez.

### 2.7 - Supprimer un fichier de données ###

- Cliquez sur **View details** du projet d'import concerné,
- Cliquez sur **Delete** du fichier xls conerné.

### 2.8 - Supprimer un fichier media ###

- Cliquez sur **View details** du projet d'import concerné,
- Cliquez sur **Delete** du fichier media concerné.