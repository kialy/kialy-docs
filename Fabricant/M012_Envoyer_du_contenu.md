# Envoyer du contenu #

## 1. Introduction ##

> Ce chapitre détaille les actions nécessaires pour envoyer du contenu d'un (ou de plusieurs) article(s) à un (ou plusieurs) utilisateur partenaire.

L'envoi de contenu :

- Concerne la totalité du contenu d'un article (données et medias),
- Peut contenir le contenu d'articles de différentes marques.

Le contenu envoyé se compose d'un fichier .zip contenant :

- Un fichier de données par langue pour tous les articles sélectionnés,
- Un dossier media par langue contenant un dossier par marque contenant un dossier par article.

#### Application des permissions ####

Les articles dont le contenu est envoyé à un utilisateur partenaire font partie du périmètre défini par les permissions de la société partenaire à laquelle l'utilisateur est rattaché.

Par conséquent, un article sélectionné ne faisant pas partie des permissions de la société de l'utilitateur de destination ne sera pas envoyé.

Vous n'avez pas à connaître les permissions pour envoyer du contenu. La plate-forme se charge d'appliquer les permissions lors de la génération de chaque fichier .zip.

## 2. Méthode ##

- Effectuer une sélection,
- Aller dans **Selection**,
- Cliquez sur **Send**,
- Choisissez le(s) utilisateur(s) à qui vous envoyé le contenu en cochant la case **Send ?**,
- Cliquez sur **Validate & Send**,
- Cliquez sur **Confirm**,
- Un email contenant un lien de téléchargement du contenu est adressé à chaque utilisateur concnerné,
- Chaque destinataire peut alors télécharger le contenu via le fichier .zip.

Il est nécessaire d'être connecté à la plate-forme pour télécharger du contenu.

La fenêtre de téléchargement du contenu s'ouvre pour vous permettre de télécharger le contenu de la sélection (optionnel).

