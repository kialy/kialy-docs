# Notifier une modification de contenu #

## 1. Introduction ##

> Ce chapitre détaille le processus automatique mis en oeuvre par la plate-forme pour informer les utilisateurs partenaires des modifications que vous effectuez sur votre contenu (données et medias).

#### Application des permissions ####

Les permissions sont appliquées dans la notification de modification.

Par conséquent, un utilisateur partenaire recevra une notification de modification concernant uniquement les articles rattachés aux noeuds de classification sur lesquels sa société a des permissions.

## 2. Généralités ##

#### Modifications concernées ####

Les modifications faisant l'objet d'une notification sont :

- Ajout / Suppression d'article,
- Ajout / Modification / Suppression de donnée (attribut et valeur),
- Ajout / Modification / Suppression de media.

#### Cadence de la notification ####

La cadence d'envoi de la notification est indépendante de votre fréquence de modification du contenu.

Par défaut, la notification est adressée chaque nuit à chaque utilisateur partenaire concerné.

Par conséquent, vous pouvez modifier votre contenu aussi souvent que vous le souhaitez dans une journée; une seule notification sera adressée.

Vous pouvez souhaiter changer la cadence des notifications.
Les cadences possibles sont :

- Information immédiate,
- Quotidienne,
- Hebdomadaire,
- Mensuelle,
- A la demande.

Cette action est un paramétrage réalisé par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    
    Objet du message : Changement de cadence de notification
    
    Corps du message : Nom de la société, cadence souhaitée.

L'équipe de Kialy informera le demandeur de la création effective par retour d'email.

#### Format de la notification ####

La notification est adressée par email.

L'email contient un rapport .xls :

- Concernant l'ensemble des Fabricants auquel l'utilisateur est inscrit,
- Délimité au périmètre défini par les permissions de sa société de rattachement,
- Contenant un onglet Review résumant le statut de tous les articles modifiés,
- Contenant un onglet Details mentionnant précisément le contenu modifié,
- Contenant un lien permettant l'ajout d'un (ou de plusieurs) article(s) à sa sélection pour téléchargement (download).

#### Format du rapport .xls ####

L'onglet Review : une ligne par article modifié.

![](http://i.imgur.com/vLrxoyJ.png)

- Manufacturer : nom du fabricant
- Code : code de l'article
- Name : nom de l'article
- Status : Créé (Created) / Modifié (Updated) / Supprimé (Deleted)
- View on Kialy : lien d'ajout à la sélection
- Date : date de la modification

L'onglet Details : une ligne par article, par langue et par modification.

![](http://i.imgur.com/nwzt08A.png)

- Manufacturer : nom du fabricant
- Code : code de l'article
- Name : nom de l'article
- Product Code : code du produit contenant l'article (facultatif)
- Product Name : nom du produit contenant l'article (facultatif)
- Object : article (Article), version de langue (Translation), données (Field), media (Media) objet de la modification
- Status : Créé (Created) / Modifié (Updated) / Supprimé (Deleted)
- Translation : version de langue modifiée
- Label : Nnom de l'élément modifié
- Valeur : valeur de l'élément modifié
- View on Kialy : lien d'ajout à la sélection
- Date : date de la modification

## 2. Processus ##

La notification de modifications est un processus automatique qui se déroule comme suit :

- Vous modifier le contenu article (par import de contenu),
- La plate-forme identifie les modifications effectuées,
- La plate-forme fait appel aux permissions de chacune des société partenaires liées à votre société,
- La plate-forme envoi la notification de modification par mail à tous les utilisateurs de la société partenaire concernées.

