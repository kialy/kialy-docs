# Gérer un compte société Partenaire #


## 1. Introduction ##

> Ce chapitre détaille les actions à réaliser concernant la création, la modification et la suppression d'un compte société Partenaire.

Une société Partenaire est une société dont le utilisateurs bénéficieront :

- De notifications lors de la modification de votre contenu,
- De la possibilité de télécharger (download) du contenu.

Elle peut donc être un client (distributeur, revendeur), comme un canal de distribution (placé de marché), comme une société soeur (filiale, importateur).

### Fichier des permissions ###

Dans le fichier des permissions, ne jamais :

- Modifier l'ID des noeuds de classification,
- Modifier le nom et la présentation des noeuds de classification,
- Supprimer de ligne,
- Changer le nom des sociétés partenaires.

Vous pouvez

- Changer le nom du fichier,
- Supprimer dans le fichier des permissions la colonne des sociétés partenaires dont vous ne traitez pas les permissions dans l'instant.

## 2. Méthode ##

La méthode de gestion d'un compte société est :

- Créer le compte société partenaire,
- Définir les permissions de la société partenaire,
- Créer les comptes utilisateurs de la société partenaire.

Afin de valider progressivement vos actions, pour chaque société partenaire, vous pourrez créer un compte utilisateur correspondant à un utilisateur de votre propre société.

### 2.1 - Créer un compte société Partenaire ###

- Cliquez sur **Resellers**,
- Cliquez sur **Create**,
- Saisissez l'email d'un utilisateur de la société partenaire.

Cas 1 : l'utilisateur mentionné existe dans la plate-forme :

- La plate-forme vous indique la société partenaire dans laquelle l'utilisateur est déjà enregistré,
- Choisissez **Link**,
- La société partenaire ainsi que tous ses utilisateurs sont alors liées à votre société,
- Le statut de cet utilisateur est **Inactif** par défaut.

Cas 2 : l'utilisateur mentionné n'existe pas dans la plate-forme :

- La plate-forme vous indique que la société n'existe pas,
- Saisissez le nom de la société partenaire (sa raison sociale de préférence),
- Choisissez **Link**,
- La société partenaire l'utilisateur concerné sont alors liées à votre société,
- Le statut de cet utilisateur est **Inactif** par défaut.

### 2.2 - Définir les persimmsions ###

Les permissions sont :

- Définies pour une société partenaire (donc toutes ses marques),
- Communes à tous ses utilisateurs,
- Par noeud de classification article.

Par défaut, lors de la création d'une société partenaire ses permissions sont globales. Tous les utilisateurs de cette société voient tous les articles de votre société.

Elles sont utilisées pour préciser le périmètre article pour lequel  ses utilisateurs :

- Recevront des notifications de modifications,
- Pourront naviguer dans le Catalog,
- Pourront télécharger (download) du contenu.

Pour définir les permissions d'une société partenaire :

- Cliquez sur **Resellers**,
- Cliquez sur **Download permissions**,
- Cliquez sur **Download permissions file**,
- Ouvrez le fichier xls téléchargé,
- Indiquez les permissions en ajoutant un 1 dans la ligne du noeud concerné et dans la colonne de la société partenaire concernée,
- Enregistrez le fichier,
- Retournez dans **Reseller**,
- Cliquez sur **Upload permissions**,
- Cliquez sur **Select permissions file (xls)**,
- Sélectionner votre fichier des permissions + cliquez sur Ouvrir,
- La plate-forme créé les permissions de la société partenaire.

        Les permissions sont descendantes, la sélection d'un noeud implique la sélection des noeuds dépendants.

### 2.3 - Délier une société partenaire ###

Le fait de délier une société partenaire à votre société, désactivera tous les droits de ladit société partenaire ainsi que de tous ses utilisateurs concernant les notifications, la vision et le téléchargement des articles de votre société.

Pour délier une société partenaire :

- Cliquez sur **View details** de la société partenaire concernée,
- Cliquez sur **Unlik**,
- Cliquez sur **Save**,
- Tous les utilisateurs de la société partenaire recevront un email les informant de l'arrêt de leur inscription aux articles de votre société.

### 2.4 - Modifier des permissions ###

La modification des permissions s'effectue par la même méthode que la définition initiale des permissions pour une société partenaire :

- Cliquez sur Resellers,
- Cliquez sur Download permissions,
- Cliquez sur Download permissions file,
- Ouvrez le fichier xls téléchargé,
- Modifiez les permissions en ajoutant / supprimant un 1 dans la ligne du noeud concerné et dans la colonne de la société partenaire concernée,
- Enregistrez le fichier,
- Retournez dans Reseller,
- Cliquez sur Upload permissions,
- Cliquez sur Select permissions file (xls),
- Sélectionner votre fichier des permissions + cliquez sur Ouvrir,
- La plate-forme créé les permissions de la société partenaire.