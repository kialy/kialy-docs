# Gérer une marque #

## 1. Introduction ##

> Ce chapitre détaille les actions à réaliser concernant la création, la modification et la suppression d'une marque rattachée à un compte société Fabricant.

Seul l'utilisateur référent de la société de rattachement est habilité à effectuer les demandes de modification et de suppression.

## 2. Méthode ##

### 2.1 - Créer une marque ###

Cette action est réalisée lors de l'import initial de contenu dans Kialy.

Le nom de la marque ainsi que son logotype sont des éléments du fichier d'import.


### 2.2 - Modifier une marque ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    
    Objet du message : Modification de marque
    
    Corps du message : Nom de la société, nom de la marque, détails de la modification à effectuer.

L'équipe de Kialy informera le demandeur de la modification effective par retour d'email.

### 2.3 - Supprimer une marque ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    
    Objet du message : Suppression de marque
    
    Corps du message : Nom de la société, nom de la marque, motif de la suppression.

L'équipe de Kialy informera le demandeur de la suppression effective par retour d'email.