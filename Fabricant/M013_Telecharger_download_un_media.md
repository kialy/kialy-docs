# Télécharger (download) un media #

## 1. Introduction ##

> Ce chapitre détaille les actions nécessaires pour télécharger uniquement un media.

#### Application des permissions ####

Les permissions ne sont pas appliquées au téléchargement de media.

## 2. Méthode ##

- Aller dans la fiche article,
- Cliquez sur **Download media** du media concerné,
- Le fichier du media concerné se télécharge dans votre dossier par défaut.