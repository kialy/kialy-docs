# Gérer un compte utilisateur Partenaire #

## 1. Introduction ##

> Ce chapitre détaille les actions à réaliser concernant la création, la modification et la suppression d'un compte utilisateur d'une société Partenaire.

## 2. Méthode ##
 
### 2.1 - Créer un nouvel utilisateur Partenaire ###

- Cliquez sur **Reseller**,
- Cliquez sur **View Details** de la société partenaire concernée,
- Cliquez sur **Add user**,
- Saisissez l'email ainsi que les Nom et Prénom de l'utilisateur,
- Cliquez sur **Save**,
- Par défaut le nouvel utilisateur des **Inactif**.

### 2.2 - Activer un utilisateur Partenaire ###

- Cliquez sur **Reseller**,
- Cliquez sur **View Details** de la société partenaire concernée,
- Activer l'utilisateur en passant son bouton sur **Actif**,
- Cliquez sur **Save**,
- L'utilisateur recevra un mail de confirmation de son inscription.

**Si vous avez créé l'utilisateur**, ce dernier recevra un email l'invitant à confirmer son inscription aux articles de votre société (dans le respect des permissions).

        L'utilisateur ne bénéficiera du service qu'à partir du moment où il aura confirmer son inscription.

**Si l'utilisateur existait déjà**, ce dernier recevra un email l'informant de son inscription aux articles de votre société (dans le respect des permissions).

        L'utilisateur ne bénéficiera du service qu'à partir du moment où il aura confirmer son inscription.

### 2.3 - Désactiver un utilisateur Partenaire ###

- Cliquez sur **Reseller**,
- Cliquez sur **View Details** de la société partenaire concernée,
- Activer l'utilisateur en passant son bouton sur **Inactif**,
- Cliquez sur **Save**,
- L'utilisateur recevra un mail de confirmation de sa désinscription.

### 2.4 - Modifier utilisateur Partenaire ###

Cette action est réalisée par l'équipe support de Kialy sous la seule condition quelle soit compatible avec toutes ses inscriptions aux Fabricants. 

Adressez un email à :

    Adresse : helpdesk@kialy.com
    Objet du message : Modification d'un compte utilisateur Partenaire
    Corps du message : Nom de la société, Nom et email de l'utilisateur concerné, détails de la modification à effectuer.

L'équipe de Kialy évaluera la faisabilité et informera le demandeur de l'action à suivre par retour d'email.

### 2.5 - Supprimer un utilisateur Partenaire ###

Cette action est réalisée par l'équipe support de Kialy sous la seule condition quelle soit compatible avec toutes ses inscriptions aux Fabricants. 

Adressez un email à :

    Adresse : helpdesk@kialy.com
    Objet du message : Suppression d'un compte utilisateur Partenaire
    Corps du message : Nom de la société, Nom et email de l'utilisateur concerné, détails de la modification à effectuer.

L'équipe de Kialy évaluera la faisabilité et informera le demandeur de l'action à suivre par retour d'email.