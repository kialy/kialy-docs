# Le fichier .xls d'import #

## 1. Introduction ##

> Ce chapitre détaille la structure ainsi que les propriété du fichier .xls utilisé pour importer manuellement du contenu (données et medias) dans la plate-forme.

Vous pouvez utiliser autant de fichiers d'import que souhaitez dès lors que chacun d'entre eux respecte la structure décrite ci-dessous.

Attention, si des fichiers possèdent des valeurs différentes pour du contenu identique, ce sont les valeurs du dernier fichier importé qui seront prises en compte.

## 2. Structure ##

#### Généralités ####

- Le fichier est au format .xls,
- Le nom du fichier est libre,
- Une ligne par article et par langue,
- Un article peut être présent plusieurs fois dans la même langue dès lors qu'il est rattaché à plusieurs noeuds de classification,
- L'ordre de classement des lignes est libre,
- Le nombre de ligne est libre,
- Les colonnes de structuration sont obligatoires,

#### Format ####

Le format du fichier d'import doit répondre aux spécifications ci-dessous.

5 colonnes sont obligatoires (en bleu dans l'exemple) et leur ordre est à conserver :

![](http://i.imgur.com/Ak529pU.png)


- Translation (obligatoire) : 1ère colonne du fichier, contient la langue. Le libellé de la langue répond à une convention de nommage (cf Les langues utilisées),
- Brand : après cette colonne indiquer le nom de la marque telle quelle apparaîtra dans la plate-forme,
- BrandLogo (obligatoire) : après cette colonne mentionner 1) le nom du fichier du logo de la marque avec son extensions. Attention, bien mettre ce fichier media dans les medias à importer lors de l'initialisation de la plate-forme. 2) les différents noeuds de classification. La classification article est libre.
- code (obligatoire) : après cette colonne mentionner le code article (identifiant unique de l'article) **Cette colonne ne peut être vide**,
- articleName / productCode / productName : ces colonnes sont facultatives mais l'usage d'au moins une (articleName par exemple) est fortement recommandé afin d'aider l'identification de l'article dans la plate-forme ainsi que dans les fichier d'export de contenu;
- Attributs : les attributs et leur valeur sont mentionnés à la suite de ces colonnes.  Un attribut est défini par 1) Son Label (ex. hauteur / disponibilité / packaging) 2) Sa valeur (ex. 15 mm / 12-05-16 / Boîte de 6),
- media (obligatoire) : après cette colonne, mentionnez les medias rattachés à l'article. Le nombre et le format est libre. Un media est mentionné par 1) Son type (ex. Image1) 2) Le nom du fichiers avec l'extension de format de fichier (ex. 45f55.jpeg). Les formats de medias les plus répandus sont acceptés (tiff, eps, jpeg, pdf, tous les formats vidéos, les formats bureautiques...).


#### Multilangue ####

Vous pouvez utiliser un fichier d'import multilangue.

Vous devrez alors mentionner la langue dans une colonne dédiée.

Cela a pour intérêt :

- De faire apparaître la langue dans la classification dans **Catalog**,
- De pouvoir définir des **permissions** par langue.

La langue sera mentionnée après le nom du logo et avant le nom des noeuds de classification.

#### Multifichier ####

Vous pouvez utiliser plusieurs fichiers d'import, chacun étant dédié à un type de contenu. Par exemple :

Un fichier de classification,
Un fichier de codes articles,
Un fichier d'attributs marketing,
Un fichier d'attributs techniques,
Un fichier d'attributs logistique,
Un fichier de rattachement des medias.

Dans ce cas respectez pour caque fichier dédié :

- La présence des colonnes obligatoires (en bleu dans l'exemple),
- Ces colonnes obligatoires ne sont pas obligatoirement suivies de colonnes de contenu,
- La présence de la colonne **code** suivi de la mention du code article.

#### Medias ####

Les medias sont mentionnés par article après la colonne **media**.

Un media peut donc être spécifique à une **Translation**.

## 3. Propriétés ##

#### La classification ####

La classification doit toujours mentionner les noeuds à l'identique. Un changement de syntaxe (casse des lettres, ajout / suppression de caractères) engendre la création d'un nouveau neoud de classification tout en conservant l'ancien noeud.

La classification doit être complète pour chaque article (du noeud principal au noeud de rattachement de l'article). Utilisez une colonne par niveau de classification.

La classification peut ne pas comprendre le même nombre de niveaux pour tous les articles.

#### Les attributs ####

Le nombre, le libellé et la valeur des attribut est libre.

Veillez à respecter le libellé des attribut dans chaque fichier d'import. Un changement de syntaxe (casse des lettres, ajout / suppression de caractères) engendre la création d'un nouvel attribut tout en conservant l'ancien attribut.

Lors de l'import, face aux modifications d'attribut, la plate-forme se comporte ainsi :

![](http://i.imgur.com/KJnwwXI.png)

#### Les medias ####

Lors de l'import, face aux modifications de media, la plate-forme se comporte ainsi :

![](http://i.imgur.com/Zd6PfpZ.png)