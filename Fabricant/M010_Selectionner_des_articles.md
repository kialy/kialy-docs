# Sélectionner des articles #

> Ce chapitre détaille le possibilités de sélectionner un ou plusieurs articles afin de soit télécharger (download) soit envoyer le contenu.

## 1. Introduction ##

Vous pouvez réaliser un sélection afin de :

- Télécharger (download) son contenu,
- Envoyer le contenu à une (ou plusieurs) utilisateurs.

L'espace Selection contient tous les articles sélectionnés, son compteur mentionne le nombre d'articles sélectionnés.

Une sélection :

- Est personnelle,
- Peut être multimarque,
- Demeure active alors que vous êtes déconnectée.

## 2. Méthode ##

### 2.1 - Sélection à partir de la liste articles ###

- Afficher la liste des articles (**Catalog** / **Browse** ou **Search**),
- Déplacer le bouton **Sélection** de l'article,
- L'article est ajouté à la sélection.

### 2.2 - Sélection à partir de la fiche article ###

- Afficher la liste des articles (**Catalog** / **Browse** ou **Search**),
- Ouvrir la fiche article (cliquez sur le **Code**),
- Cliquez sur **Add to selection**,
- L'article est ajouté à la sélection.

### 2.3 - Sélection à partir de SELECTION ###

- Aller dans **Selection**,
- Cliquez sur **Add articles**,
- Saisissez le(s) code(s) article(s) souhaité(s),
- Cliquez sur **Add to selection**,
- L'article est ajouté à la sélection.

### 2.4 - Modifier une sélection ###

#### A partir de la liste articles ####

- Afficher la liste des articles (**Catalog** / **Browse** ou **Search**),
- Déplacer le bouton **Sélection** de l'article,
- L'article est supprimé de la sélection.

#### A partir de la fiche article ####

- Afficher la liste des articles (**Catalog** / **Browse** ou **Search**),
- Ouvrir la fiche article (cliquez sur le **Code**),
- Cliquez sur **Remove from selection**,
- L'article est supprimé de la sélection.

#### A partir de Selection ####

- Aller dans **Selection**,
- Déplacer le bouton **Sélection** de l'article,
- L'article est supprimé de la sélection mais demeure dans la liste des articles de la sélection.

### 2.5 - Supprimer une sélection ###

- Aller dans **Selection**,
- Cliquez sur **Clear**,
- Cliquez sur **Confirm**,
- La sélection est supprimée.