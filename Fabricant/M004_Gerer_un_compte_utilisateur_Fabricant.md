# Gérer un utilisateur Fabricant #

## 1. Introduction ##

> Ce chapitre détaille les actions à réaliser concernant la création, la modification et la suppression d'un compte utilisateur rattaché à une société Fabricant.

Seul l'utilisateur référent de la société de rattachement est habilité à effectuer ces opérations.

## 2. Méthode ##

### 2.1 - Créer un compte utilisateur Fabricant ###


Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    Objet du message : Création d'un compte utilisateur Fabricant
    Corps du message : Nom de la société, Nom et email de l'utilisateur concerné.

L'équipe de Kialy informera le demandeur et l'utilisateur de la création effective par retour d'email.

### 2.2 - Modifier un compte utilisateur Fabricant ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    Objet du message : Modification d'un compte utilisateur Fabricant
    Corps du message : Nom de la société, Nom et email de l'utilisateur concerné, détails de la modification à effectuer.

L'équipe de Kialy informera le demandeur et l'utilisateur de la modification effective par retour d'email.

### 2.3 - Supprimer un compte utilisateur Fabricant ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    Objet du message : Suppression d'un compte utilisateur Fabricant
    Corps du message : Nom de la société, Nom et email de l'utilisateur concerné, motif de la suppression.

L'équipe de Kialy informera le demandeur et l'utilisateur de la suppression effective par retour d'email.