# Gérer un compte société Fabricant #


## 1. Introduction ##

> Ce chapitre détaille les actions à réaliser concernant la création, la modification et la suppression d'un compte société Fabricant.

Seul l'utilisateur référent de la société est habilité à effectuer ces actions..

## 2. Méthode ##

### 2.1 - Créer un compte société Fabricant ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    
    Objet du message : Création de société
    
    Corps du message : Nom de la société, Nom de l'utilisateur référent.

L'équipe de Kialy informera le demandeur de la création effective par retour d'email.

### 2.2 - Modifier un compte société Fabricant ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    
    Objet du message : Modification de société
   
    Corps du message : Nom de la société, Nom de l'utilisateur référent, détails de la modification à effectuer.

L'équipe de Kialy informera le demandeur de la modification effective par retour d'email.

### 2.3 - Supprimer un compte société Fabricant ###

Cette action est réalisée par l'équipe support de Kialy. Adressez un email à :

    Adresse : helpdesk@kialy.com
    
    Objet du message : Suppression de société
    
    Corps du message : Nom de la société, Nom de l'utilisateur référent, motif de la suppression.

L'équipe de Kialy informera le demandeur de la suppression effective par retour d'email.