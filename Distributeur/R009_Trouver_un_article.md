# Trouver un article #

## 1. Introduction ##

> Ce chapitre détaille les possibilités de navigation dans votre catalogue.

Le menu **Catalog** vous donne accès :

- Aux marques auxquelles votre société est liée,
- Aux articles classés dans ces marques pour lesquels votre société a des permissions,
- Aux données et medias rattachés à ces articles.

**Gardez à l'esprit que le résultat de vos actions est toujours filtré suivant les permissions que possède votre société sur la marque.**

## 2. Méthode ##

### 2.1 - Accéder aux marques ###

- Cliquez sur **Catalog**,
- Vous accédez directement à toutes les marques des sociétés auxquelles vous êtes inscrit(e).

### 2.2 - Accéder aux articles ###

- Cliquez sur **Browse** d'une des marques,
- Vous accédez directement à la liste des articles de cette marque.

### 2.3 - Accéder au contenu article ###

#### Via la classification ####

- Cliquez sur le dossier du noeud de la classification développe cette classification en conservant l'affichage de la liste de tous les articles.

- Cliquez sur le nom du noeud de la classification de développe pas cette classification et affiche uniquement la liste des articles contenus dans le noeud concerné.

Si la classification existe en plusieurs langues :

- Les noeuds de classification apparaissent dans toutes les langues disponibles,
- Les articles possédant du contenu dans cette version apparaissent dans la liste des articles.

#### Via la liste des articles ####

- Cliquez sur le **Code** article,
- Ouvre la fiche article.

#### Via la recherche ####

- Saisissez le **Code** article dans l'espace Search,
- Réduit la liste article au code saisi.