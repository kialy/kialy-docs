# Télécharger (download) un media #

## 1. Introduction ##

> Ce chapitre détaille les actions nécessaires pour télécharger uniquement un media.

**Gardez à l'esprit que le résultat de vos actions est toujours filtré suivant les permissions que possède votre société sur la marque.**

## 2. Méthode ##

- Aller dans la fiche article,
- Cliquez sur **Download media** du media concerné,
- Le fichier du media concerné se télécharge dans votre dossier par défaut.