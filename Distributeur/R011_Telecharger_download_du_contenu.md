# Télécharger (download) du contenu #

## 1. Introduction ##

> Ce chapitre détaille les actions nécessaires pour télécharger le contenu d'un (ou de plusieurs) article(s).

Le téléchargement de contenu :

- Concerne la totalité du contenu d'un article (données et medias),
- Peut contenir le contenu d'articles de différentes marques,
- Est considéré pour un usage interne à votre entreprise.

Le contenu téléchargé se compose d'un fichier .zip contenant :

- D'un fichier de données par langue pour tous les articles sélectionnés,
- D'un dossier media par langue contenant un dossier par marque contenant un dossier par article.

**Gardez à l'esprit que le résultat de vos actions est toujours filtré suivant les permissions que possède votre société sur la marque.**

## 2. Méthode ##

- Effectuer une sélection,
- Aller dans **Selection**,
- Cliquez sur **Download**,
- Cliquez sur **Download selection** dans la fenêtre de téléchargement,
- Le fichier .zip de contenu se télécharge dans votre dossier par défaut,
- Cliquez sur **Cancel** pour faire disparaître la fenêtre de téléchargement,
- Décompressez le fichier .zip de contenu.

Un email contenant le lien de téléchargement du .zip vous a également été adressé.

Il est nécessaire d'être connecté à la plate-forme pour télécharger du contenu.