# Se connecter / Se déconnecter #


## 1. Introduction ##

> Ce chapitre détaille la méthode de connexion / déconnexion à la plate-forme.

## 2. Méthode ##

### 2.1 - Connexion ###

-	Aller sur **https://kly.io/login**,
-	Saisissez votre adresse mail,
-	Saisissez votre mot de passe,
-	Cliquez sur **Log In**,
-	Vous arrivez sur votre **Dashboard**,
-	Vous êtres connecté(e).

### 2.2 - Déconnexion ###

-	Cliquez sur votre nom d'utilisateur (en haut à droite),
-	Cliquez sur **Logout**,
-	Vous arrivez sur l'écran de connexion,
-	Vous êtres déconnecté(e).