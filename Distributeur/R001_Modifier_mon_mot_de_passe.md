# Modifier mon mot de passe #

## 1. Introduction ##

> Ce chapitre défini les modalités de modification de votre mot de passe d'accès à Kialy.

Le mot de passe utilisateur est généré automatiquement par la plate-forme et est garanti unique.

Votre identifiant demeure votre adresse email.

## 2. Méthode ##

-	Aller sur https://kly.io/login,
-	Cliquez sur «Forgot your password? »,
-	Saisissez votre adresse mail,
-	Cliquez sur « Send me a new password »,
-	La plate-forme vous adresse un nouveau mot de passe par email.