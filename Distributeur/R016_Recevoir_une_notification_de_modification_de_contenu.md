# Notifier une modification de contenu #

## 1. Introduction ##

> Ce chapitre détaille le processus automatique mis en oeuvre par la plate-forme pour vous informer des modifications que vos fournisseurs feront dans leur contenu (données et medias).

Vous ne recevrez de notifications d'une marque que si :

- Vous avez été invité mar ladite marque,
- Vous avez, le cas échéant, confirmé votre inscription.

**Gardez à l'esprit que le périmètre de notification est toujours filtré suivant les permissions que possède votre société sur la marque.**

## 2. Généralités ##

#### Modifications concernées ####

Les modifications faisant l'objet d'une notification sont :

- Ajout / Suppression d'article,
- Ajout / Modification / Suppression de donnée (attribut et valeur),
- Ajout / Modification / Suppression de media.

#### Cadence de la notification ####

La cadence d'envoi de la notification est indépendante de la fréquence de modification du contenu.

Par défaut, la notification vous est adressée chaque nuit.

Chaque farbicant peut adapter la cadence des notifications. Les cadences possibles sont :

- Information immédiate,
- Quotidienne,
- Hebdomadaire,
- Mensuelle,
- A la demande.

#### Format de la notification ####

La notification vous est adressée par email.

L'email contient un rapport .xls :

- Concernant l'ensemble des Fabricants auquel vous êtes inscrit,
- Contenant un onglet Review résumant le statut de tous les articles modifiés,
- Contenant un onglet Details mentionnant précisément le contenu modifié,
- Contenant un lien vous permettant l'ajout d'un (ou de plusieurs) article(s) à votre sélection pour téléchargement (download) ultérieur.

#### Format du rapport .xls ####

L'onglet Review : une ligne par article modifié.

![](http://i.imgur.com/vLrxoyJ.png)

- Manufacturer : nom du fabricant
- Code : code de l'article
- Name : nom de l'article
- Status : Créé (Created) / Modifié (Updated) / Supprimé (Deleted)
- View on Kialy : lien d'ajout à la sélection
- Date : date de la modification

L'onglet Details : une ligne par article, par langue et par modification.

![](http://i.imgur.com/nwzt08A.png)

- Manufacturer : nom du fabricant
- Code : code de l'article
- Name : nom de l'article
- Product Code : code du produit contenant l'article (facultatif)
- Product Name : nom du produit contenant l'article (facultatif)
- Object : article (Article), version de langue (Translation), données (Field), media (Media) objet de la modification
- Status : Créé (Created) / Modifié (Updated) / Supprimé (Deleted)
- Translation : version de langue modifiée
- Label : Nnom de l'élément modifié
- Valeur : valeur de l'élément modifié
- View on Kialy : lien d'ajout à la sélection
- Date : date de la modification

